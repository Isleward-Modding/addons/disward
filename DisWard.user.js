// ==UserScript==
// @name         DisWard
// @namespace    Isleward.Addon
// @version      1.4
// @description  QoL plugin for dungeons and boss tracking
// @author       disasm.me
// @match        https://play.isleward.com/*
// @match        https://ptr.isleward.com/*
// @grant        GM_notification
// @require https://cdn.jsdelivr.net/combine/npm/@violentmonkey/dom@2,npm/@violentmonkey/ui@0.7
// ==/UserScript==


DISWARD_PANELS = {
    "monsterCountAddon": 0,
    "panelControlAddon": 0,

}

// FIXME: allow users to set their default values
DISWARD_SETTINGS = {
    "show_monsterCountAddon": true,
    //"show_panelControlAddon": true,
    "notify_BossAddon": true,
    "notify_templeEnter": true,
    "notify_Sounds": true,
}


DISWARD_VERSION = 1.4

PRESERVE_ADDONS = []

let addons_register = function (addon_body) {
    PRESERVE_ADDONS.push(addon_body)
}


function initPanel(name, panel, {top = '400px', left = '10px'} = {}) {
    // init panel
    panel.wrapper.style.top = top;
    panel.wrapper.style.left = left;
    panel.body.style.lineHeight = '7px';
    panel.body.style.backgroundColor = "#3A3B4A";
    panel.body.style.color = "#8DA7A7";
    panel.setMovable(true);

    panel.wrapper.addEventListener('mouseup', () => {
        //console.log(panel.wrapper.offsetTop, panel.wrapper.offsetLeft)
        localStorage.setItem('disward_' + name + '_top', panel.wrapper.offsetTop + "px");
        localStorage.setItem('disward_' + name + '_left', panel.wrapper.offsetLeft + "px");
    });

    // add it to isleward ui container
    const uiContainer = document.querySelector('.ui-container');
    uiContainer?.appendChild(panel.root);


    const top_pos = localStorage.getItem('disward_' + name + '_top');
    const left_pos = localStorage.getItem('disward_' + name + '_left');

    if (top_pos && left_pos) {
        //console.log(top_pos, left_pos)
        panel.wrapper.style.top = top_pos;
        panel.wrapper.style.left = left_pos;
    }

    return panel;
}


/* Boss notification addon - notifies when monster is up */
addons.register({
    forBrowserNotification: ["Stinktooth", "m'ogresh", "Lord Squash"], // "*everyone*"
    init: function (events) {
        events.on('onGetObject', this.onGetObject.bind(this));
    },

    onGetObject: function (msg) {
        var self = this;

        if (!DISWARD_SETTINGS["notify_BossAddon"]) return;

        if (msg.sheetName === "mobs" || msg.sheetName === "bosses" || this.forBrowserNotification.includes(msg.name)) {
            if (self.forBrowserNotification.includes(msg.name) || self.forBrowserNotification.includes("*everyone*")) {
                GM_notification({
                    title: "[DisasmWard] Boss respawned",
                    text: msg.name,
                    image: 'https://i.stack.imgur.com/geLPT.png'
                });
            }

        }
    }
});

/* Music highlighter - shows source of laugh */
addons.register({
    init: function (events) {
        this.events = null;
        events.on('onPlaySoundAtPosition', this.onPlaySoundAtPosition.bind(this));
    },

    onPlaySoundAtPosition: function (msg) {
        var self = this;

        if (!DISWARD_SETTINGS["notify_Sounds"]) return;

        if (!this.events)
            this.events = require("js/system/events");

        this.events.emit("onGetObject", {
            "components": [
                {
                    "type": "particles",
                    "blueprint": {
                        "scale": {
                            "start": {
                                "min": 5,
                                "max": 5
                            },
                            "end": {
                                "min": 5,
                                "max": 5
                            }
                        },
                        "speed": {
                            "start": {
                                "min": 2,
                                "max": 12
                            },
                            "end": {
                                "min": 0,
                                "max": 4
                            }
                        },
                        "color": {
                            "start": ["ff2d00", "ff2d00"],
                            "end": ["ff2d00", "ff2d00"]
                        },
                        "spawnType": "circle",
                        "spawnCircle": {
                            "x": 0,
                            "y": 0,
                            "r": 1
                        },
                        "randomScale": true,
                        "randomColor": true,
                        "randomSpeed": true,
                        "chance": 1.0
                    }

                }
            ],
            "id": "diswardSoundParticles",
            "x": msg.position.x,
            "y": msg.position.y
        })
        let events = this.events;
        setTimeout(() => {
            events.emit("onGetObject", {"id": "diswardSoundParticles", "destroyed": true}, 2000);
        })
    }
});


/* Horse Switcher addon*/
addons.register({
    init: function (events) {
        events.on('onGetItems', this.onGetItems.bind(this));
    },


    onGetItems: function (msg) {
        var self = this;

        isCurrentQuickSlotNeedRepoace = false;
        isCurrentQuickSlotMount = false;

        for (let item_id in msg) {
            item = msg[item_id]
            // check that current item in quickslot is mount, otherwise we should not turn on this functionality
            if ('quickSlot' in item) {
                self.isCurrentQuickSlotMount = item.type === "mount";
                self.isCurrentQuickSlotCd = item.cd;
                $(".quickItem .info").text("R " + item.cd);
                break
            }
        }
        if (self.isCurrentQuickSlotCdOk === 0)
            return

        if (!self.isCurrentQuickSlotMount)
            return

        // now let's find the best horse
        var bestHorseId = -1;
        var bestHorseCd = 999999;
        var currentHorseId = -1;
        for (let item_id in msg) {
            item = msg[item_id]
            if (item.type === "mount") {
                //console.log("mount", item)
                if (item.cd < bestHorseCd) {
                    bestHorseId = item.id;
                    bestHorseCd = item.cd;
                }
                if ("quickSlot" in item) {
                    currentHorseId = item.id
                }
            }
        }
        if (bestHorseId === -1)
            return
        if (bestHorseId === currentHorseId)
            return

        if (!this.client) this.client = require("js/system/client");
        //console.log("New good candidate", item)
        this.client.request({
            cpn: 'player',
            method: 'performAction',
            data: {cpn: 'equipment', method: 'setQuickSlot', data: {"itemId": bestHorseId, "slot": 0}}
        });


    },
});


/* Count of monsters addon */
addons.register({

    normalMobTpl: `<div>$MOB$ [$COUNT$] $XYs$</div><br>`,
    eliteMobTpl: `<div style="color: blue;"> $MOB$ [$COUNT$] $XYs$</div><br>`,
    bossMobTpl: `<div style="color: red;"> $MOB$ [$COUNT$] $XYs$</div><br>`,

    elites: ["The Muck Prince"],
    bosses: ["Stinktooth", "m'ogresh", "Bera the Blade", "Thumper", "lordSquash"],
    ignore: ["Guard", "Pig", "Goat", "Cow", "Vikar", "Estrid", "Luta"],

    pumpkinEvent: ["Risen Pumpkin", "Risen Eggplant", "Haunted Pumpkin", "Haunted Eggplant", "Pumpkin", "Tiny Pumpkin", "Giant Pumpkin"],


    init: function (events) {
        this.monstersPanel = VM.getPanel({
            content: VM.h('div', {}, "Monster Track by disasm.me"),
            theme: 'dark'
        });
        this.monstersPanel = initPanel("monstersPanel", this.monstersPanel, {top: '200px', left: '10px'});

        DISWARD_PANELS["monsterCountAddon"] = this.monstersPanel;


        this.tracked_mobs = {}; // name: [id1, id2, ...]
        this.monster_locations = {}; // id: {x, y}

        events.on('onGetObject', this.onGetObject.bind(this));
        events.on('onGetMap', this.onGetMap.bind(this));
    },

    onGetMap: function (msg) {
        // we moved to new map, so need to clear
        this.tracked_mobs = {};
        this.monster_locations = {};

    },

    onGetObject: function (msg) {
        var self = this;
        // Checking if a monster was destroyed
        if (msg.destroyed) {
            for (let name in self.tracked_mobs) {
                let monsterIdIndex = self.tracked_mobs[name].indexOf(msg.id);

                // If monster id is found in the array, remove it
                if (monsterIdIndex !== -1) {
                    self.tracked_mobs[name].splice(monsterIdIndex, 1);
                }
            }

            // also remove from tracked ids
            let monsterIdIndex = self.monster_locations.valueOf(msg.id);
            if (monsterIdIndex !== -1) {
                delete self.monster_locations[monsterIdIndex];
            }
        }

        if (msg.sheetName === "mobs" || msg.sheetName === "bosses") {
            if (!self.ignore.includes(msg.name)) {
                this.track(msg)
            }
        }
        // In pumpkin event there is a strange type of monsters - they have no sheetName
        if (self.bosses.includes(msg.id) || this.pumpkinEvent.includes(msg.name)) {
            if (!self.ignore.includes(msg.name)) {
                this.track(msg)
            }
        }

        if (msg.hasOwnProperty('id') && msg.hasOwnProperty('x') && msg.hasOwnProperty('y')) {
            this.monster_locations[msg.id] = {
                x: msg.x,
                y: msg.y
            };
            this.redrawPanel();
            this.albinoCheck();
        }
    },

    redrawPanel() {
        var self = this;
        var panelContent = '';

        for (let name in self.tracked_mobs) {
            let mobCount = self.tracked_mobs[name].length;
            let mobTemplate;
            let mobXYs = '';

            for (let id of self.tracked_mobs[name]) {
                let loc = self.monster_locations[id];
                let locString = loc ? ` ${loc.x}x${loc.y}` : '';
                mobXYs += locString + ',';
            }
            mobXYs = mobXYs.slice(0, -1); // Remove trailing comma

            if (self.bosses.includes(name)) {
                mobTemplate = self.bossMobTpl;
            } else if (self.elites.includes(name)) {
                mobTemplate = self.eliteMobTpl;
            } else {
                mobTemplate = self.normalMobTpl;
            }

            panelContent += mobTemplate
                .replace('$MOB$', name)
                .replace('$COUNT$', mobCount).replace('$XYs$', mobXYs);
        }
        this.monstersPanel.body.innerHTML = panelContent;
    },

    albinoCheck() {
        var self = this;

        var albinoSerpentMessage = '';

        const locations = {
            'Room 1': {minX: 20, minY: 43, maxX: 30, maxY: 53},
            'Room 2': {minX: 62, minY: 43, maxX: 72, maxY: 53},
            // todo: r3 location
            'Corridor r1-r2': {minX: 32, minY: 46, maxX: 60, maxY: 50},
            'Corridor r2-r3': {minX: 65, minY: 24, maxX: 69, maxY: 42},
            'Room 4': {minX: 20, minY: 13, maxX: 30, maxY: 23},
            'Room 5': {minX: 4, minY: 18, maxX: 19, maxY: 26}
        };

        for (let id of self.tracked_mobs['Albino Serpent'] || []) {
            let loc = self.monster_locations[id];
            if (loc) {
                for (let location in locations) {
                    let {minX, minY, maxX, maxY} = locations[location];
                    if (loc.x >= minX && loc.x <= maxX && loc.y >= minY && loc.y <= maxY) {
                        albinoSerpentMessage = albinoSerpentMessage + `<div style="color: red;">Albino serpent is in the ${location}!</div><br>`;
                        break;
                    }
                }
            }
        }

        this.monstersPanel.body.innerHTML = this.monstersPanel.body.innerHTML + albinoSerpentMessage;
    },

    track: function (msg) {
        var self = this;

        // Add new monster to the tracked list
        if (!self.tracked_mobs[msg.name]) {
            self.tracked_mobs[msg.name] = [];
        }
        let monsterIdIndex = self.tracked_mobs[msg.name].indexOf(msg.id);
        if (monsterIdIndex === -1) {
            self.tracked_mobs[msg.name].push(msg.id);
        }

        this.redrawPanel();
        this.albinoCheck();


    },
});


/* Menu */
addons.register({
    init: function (events) {
        this.panelControlPanel = VM.getPanel({
            content: VM.h('div', {}, ""),
            theme: 'dark'
        });

        this.panelControlPanel = initPanel("panelControlPanel", this.panelControlPanel, {top: '400px', left: '10px'});


        DISWARD_PANELS["panelControlAddon"] = this.panelControlPanel;


        document.addEventListener('keydown', (event) => {
            if (event.shiftKey && (event.key === 't' || event.key === 'T')) {
                // Assuming the panel is accessible as `this.panelName`
                if (!DISWARD_PANELS["panelControlAddon"].wrapper.style.display)
                    DISWARD_PANELS["panelControlAddon"].wrapper.style.display = "none";
                else DISWARD_PANELS["panelControlAddon"].wrapper.style.display = "";
            }
        });
        this.updatePanelControl();
    },

    updatePanelControl: function () {
        var html = `<div>DisWard ` + DISWARD_VERSION + ` (Shift+T)<\div><br>`

        // Create checkboxes for notify and show settings
        for (let setting in DISWARD_SETTINGS) {
            if (setting.startsWith('notify_')) {
                html += `
                    <input type="checkbox" id="${setting}" ${DISWARD_SETTINGS[setting] ? 'checked' : ''}>
                    <label for="${setting}">${setting}</label><br>
                `;
            }
            if (setting.startsWith('show_')) {
                html += `
                    <input type="checkbox" id="${setting}" ${DISWARD_SETTINGS[setting] ? 'checked' : ''}>
                    <label for="${setting}">${setting}</label><br>
                `;
            }
        }
        this.panelControlPanel.body.innerHTML = html;


        // Add event listeners for show/hide panels
        for (let panel in DISWARD_PANELS) {
            if (panel === "panelControlAddon") continue;
            document.getElementById('show_' + panel).addEventListener('change', (e) => {
                DISWARD_SETTINGS['show_' + panel] = e.target.checked;
                if (e.target.checked) {
                    DISWARD_PANELS[panel].wrapper.style.display = "";
                } else {
                    DISWARD_PANELS[panel].wrapper.style.display = "none";
                }
            });
        }
        // Add event listeners for notify settings
        for (let setting in DISWARD_SETTINGS) {
            if (setting.startsWith('notify_')) {
                document.getElementById(setting).addEventListener('change', (e) => {
                    DISWARD_SETTINGS[setting] = e.target.checked;
                });
            }
        }

    }
});
