### 1.4
* Previously addon was activating if online player list was inited successfully and there are 4+ player. Now it's lifted
* User location addon was removed - it was implemented in Isleward 0.13.0
* Notifications that someone entered Nyx Temp broke, will be fixed soon(TM)
* Added location display for Pumpkin mobs and pumpkins itself - so you can indicate that there is a big pumpkin nearby!
* Added Pumpkin Boss to notifications - if you stand on its ground and it will spawn - you will be notified via browser
* Added Soul Lantern activation position highligher (that loud hahahahah). It will help to guess who activated it.

### 1.3
* Now addon will activate their hooks when there are 4+ players in online players list
(it's a reliable indicator that online player list was inited nicely).

### 1.2
* Display cooldown for quickslot item
* Horse rework: auto-switching to mount with the least cooldown. It would work if you have 2+ types of mounts.

### 1.1
* Rewrote panel init
  * removed colors duplication
  * storing panel position after its move
* Notification when someone enters temple Of Gaekatla
* Addon panel can be hidden by Shift+T

### 1.0
* Initial release on gitlab
