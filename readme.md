# DisWard
Several addons that allows you to track bosses, current players and other stuff.

The author: <b>disasm.me</b> (discord) / <b>disassembly</b> (isleward)

[Changelog](changelog.md)

## Install
1. Install the TamperMonkey
2. Install the addon: 
   * With autoupdate feature: [DisWard.autoupdate.user.js](https://gitlab.com/Isleward-Modding/addons/disward/-/raw/main/DisWard.autoupdate.user.js)
   * Only current version: [DisWard.user.js](https://gitlab.com/Isleward-Modding/addons/disward/-/raw/main/DisWard.user.js)
3. Reload Isleward

## Features

### Show the location of all online users (this feature was removed since isleward 0.13.0)
![img.png](docs/features1.png)

#### This feature will help you to know who is in est / temple right now

Tip: sometimes you will see `???` instead of name of maps.

![img.png](docs/features2.png)

It's because server returned info that user changed location by location ID, without the name.
So now worry, it's normal, otherwise changing location actualization will be much slower.
It will be auto-fixed in ~1 min when the general event about all users location will be sent.

### Browser notification that mog / stink is up

![img.png](docs/features3.png)

#### This feature allows you to be half-afk while waiting for boss to spawn. It relies on the appearance of bosses near you.

It will help you to play another game / to program / to draw / to do other office stuff / etc. on boss farm.

### Monsters around you

![img.png](docs/features5.png)

This feature allows you to know some info about current monsters.

![img.png](docs/features6.png)

The extra temple-related feature (unfinished, WIP) - temple albino hunt on rooms 1, 4, 5.

#### Quickslot tweaks

![img.png](docs/features7.png)

1. It shows cooldown for your item
2. For horses - autoswitch to the horse with the least cooldown

#### Soul Lantern activation position highligher (that loud hahahahah).

It will help to track who activated it :)

![img.png](docs/features8.png)

## Helping to project

Feel free create issues, pull requests to this project, or write me on discord.

If you have an extra ideas for addons, you can contact me - let's look what we can do.

The best help will be if you will buy isleward patreon (or pay for mine, hehe).

## See also

If you like the functionality of this addon,
consider to check [Waddon](https://gitlab.com/Isleward-Modding/addons/waddon) - cool general purpose addon for isleward