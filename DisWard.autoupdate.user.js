// ==UserScript==
// @name         DisWard.autoupdate
// @namespace    Isleward.Addon
// @version      1.3
// @description  Wrapper that automatically loads the latest version os DisWard
// @author       disasm.me
// @match        https://play.isleward.com/*
// ==/UserScript==

(function() {
    var scriptElement = document.createElement( "script" );
    scriptElement.type = "text/javascript";
    scriptElement.src = "https://cdn.rawgit.com/Isleward-Modding/addons/disward/main/DisWard.js";
    document.body.appendChild( scriptElement );
})();
